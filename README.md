--------------------------------------------------------------------------------
# He3Labs Catapult Local Dev Environment
--------------------------------------------------------------------------------
For right now, we're basing this off of ubuntu/bionic64.  This latest version uses the `he3labs/catapult-dev` vagrant box from https://app.vagrantup.com/he3labs/boxes/catapult-dev/versions/0.0.1

--------------------------------------------------------------------------------
## First time spin up
--------------------------------------------------------------------------------
```
vagrant up
```

--------------------------------------------------------------------------------
## Collaboration
--------------------------------------------------------------------------------

We're tracking this repository on our public gitlab.com group: https://gitlab.com/he3labs-public/catapult-server-vagrant

--------------------------------------------------------------------------------
## Running
--------------------------------------------------------------------------------
```
cd /vagrant/catapult-server/_build

perl -pi -e 's/^pluginsDirectory.*/pluginsDirectory = .\/bin/g' ../resources/config-user.properties

mkdir ../data
cp ../tools/nemgen/resources/mijin-test.properties .
mkdir -p ../seed/mijin-test
./bin/catapult.tools.nemgen mijin-test.properties
mkdir -p ../data/00000
cp ../seed/mijin-test/00000/00001.dat ../data/00000/

bin/catapult.server
```
