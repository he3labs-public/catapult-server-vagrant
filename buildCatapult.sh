#!/bin/bash

################################################################################
# sync time
################################################################################
sudo /usr/sbin/VBoxService --timesync-set-start

################################################################################
# catapult
################################################################################

cd /vagrant
if [ ! -d "/vagrant/catapult-server" ]; then
  git clone https://github.com/nemtech/catapult-server.git
fi
cd catapult-server
git reset --hard HEAD

rm -Rf _build
mkdir _build
cd _build

# ZeroMQ_DIR
export ZeroMQ_DIR=/usr/local
# BOOST_ROOT (skipped for packaged boost)
# export BOOST_ROOT=/usr/include
# ROCKSDB_ROOT_DIR
export ROCKSDB_ROOT_DIR=/usr
# LIBBSONCXX_DIR
export LIBBSONCXX_DIR=/usr/local
# LIBMONGOCXX_DIR
export LIBMONGOCXX_DIR=/usr/local
# GTEST_ROOT
export GTEST_ROOT=/usr
# cppzmq_DIR
export cppzmq_DIR=/usr/local
# PYTHON_EXECUTABLE
export PYTHON_EXECUTABLE=/usr/bin/python3

cmake -DCMAKE_BUILD_TYPE=RelWithDebugInfo ..

make publish; make
